//
//  ViewController.m
//  MantleTest
//
//  Created by Puneet Agarwal on 4/3/16.
//  Copyright © 2016 ps. All rights reserved.
//

#import "ViewController.h"
#import "ABC.h"
#import "JSONDict.h"
@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
//    NSDictionary *JSONDictionary = @{
//                                     @"name": @"john",
//                                     @"id" : @"adsasdasdasdasd"
//                                     };
//    
//    ABC *abc = [MTLJSONAdapter modelOfClass:ABC.class fromJSONDictionary:JSONDictionary error:NULL];
//    NSLog(@"Identifier: %@",abc.Identifier);
    
    //Getting s3 data
//    NSData *s3Data = [NSData dataWithContentsOfURL:[NSURL URLWithString:@"https://m1re6iqld7.execute-api.us-east-1.amazonaws.com/Test/lists3buckets"]];
//    
//    NSError *error = nil;
//    NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:s3Data options:0 error:&error];
//    NSLog(@"%@",dict);
//    
//    JSONDict *jsonDict = [MTLJSONAdapter modelOfClass:JSONDict.class fromJSONDictionary:dict error:NULL];
//    NSLog(@"jsonDict.response: %@",jsonDict.response);
    
    
    //Getting s3 data using NSURLSessionDataTask
    NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfig];
    
    NSURLSessionDataTask *jsonDataTask = [session dataTaskWithURL:[NSURL URLWithString:@"https://m1re6iqld7.execute-api.us-east-1.amazonaws.com/Test/lists3buckets"] completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error)
    {
        NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
        NSLog(@"%@",dict);
        
        JSONDict *jsonDict = [MTLJSONAdapter modelOfClass:JSONDict.class fromJSONDictionary:dict error:NULL];
        NSLog(@"jsonDict.response: %@",jsonDict.response);
    }];
    [jsonDataTask resume];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
