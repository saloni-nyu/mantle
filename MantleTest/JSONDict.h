//
//  JSONDict.h
//  MantleTest
//
//  Created by Saloni Agarwal on 4/3/16.
//  Copyright © 2016 ps. All rights reserved.
//

#import "MTLModel.h"
#import <Mantle/Mantle.h>

@interface JSONDict : MTLModel<MTLJSONSerializing>
@property (nonatomic,strong)NSArray *response;
@end
