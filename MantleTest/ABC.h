//
//  ABC.h
//  MantleTest
//
//  Created by Puneet Agarwal on 4/3/16.
//  Copyright © 2016 ps. All rights reserved.
//

#import <Mantle/Mantle.h>

@interface ABC : MTLModel<MTLJSONSerializing>
@property (nonatomic, strong) NSString *Identifier;
@property (nonatomic, strong) NSString *Name;
@end
