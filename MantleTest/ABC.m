//
//  ABC.m
//  MantleTest
//
//  Created by Puneet Agarwal on 4/3/16.
//  Copyright © 2016 ps. All rights reserved.
//

#import "ABC.h"

@implementation ABC
+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    
    return @{
             @"Identifier" : @"id",
             @"Name" : @"name"
             };
}
@end
