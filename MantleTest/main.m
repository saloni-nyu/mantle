//
//  main.m
//  MantleTest
//
//  Created by Puneet Agarwal on 4/3/16.
//  Copyright © 2016 ps. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
